/* punzip: Parallel unzip, a terrible hack to speed up unzipping large
 * zip files by spawning children to decompress each file in a zip.
 *
 * Authors: Rob Kendrick <rob.kendrick@codethink.co.uk>
 *          Jonathan Sambrook <jonathan.sambrook@codethink.co.uk>
 *
 * Copyright 2021 Codethink Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/socket.h>
#include <archive.h>
#include <archive_entry.h>

#include <sched.h>

static bool opt_quiet = false;
static bool opt_listonly = false;
static int opt_nprocs = 0;
extern char *optarg;
extern int optind, opterr, optopt;

static void usage(FILE *f, const char progname[static 1])
{
    fprintf(f, "usage: %s [options] <file.zip>\n", progname);
    fprintf(f, "\t-d dir\tdestination directory\n");
    fprintf(f, "\t-h\tthis help\n");
    fprintf(f, "\t-l\tlist zip contents\n");
    fprintf(f, "\t-q\tbe quiet\n");
    fprintf(f, "\t-j n\tuse up to n CPUs (or inf if n is omitted)\n");
}

static int get_nprocs(int def)
{
    /* we could use sysconf or get_nprocs, but this counts the number
     * of CPUs in the system, not the number of CPUs we're configured
     * to use.
     */
    cpu_set_t mask;

    CPU_ZERO(&mask);
    if (sched_getaffinity(getpid(), sizeof(mask), &mask) == -1) {
        return def;
    }

    return CPU_COUNT(&mask);
}

static int get_num_files(const char fn[static 1])
{
    struct archive *a;
    struct archive_entry *e;
    int count = 0;

    a = archive_read_new();
    archive_read_support_filter_all(a);
    archive_read_support_format_all(a);

    int infd = open(fn, O_RDONLY);
    if (infd == -1) {
        fprintf(stderr, "unable to open %s (%s)\n", fn, strerror(errno));
        return -1;
    }

    if (archive_read_open_fd(a, infd, 1024 * 8) != ARCHIVE_OK) {
        fprintf(stderr, "unable to open %s: %s\n", fn, archive_error_string(a));
        close(infd);
        return -1;
    }

    while (true) {
        int r = archive_read_next_header(a, &e);
        if (r == ARCHIVE_EOF) {
            break;
        }
        if (r == ARCHIVE_RETRY) {
            continue;
        }
        if (r < ARCHIVE_OK) {
            fprintf(stderr, "error enumerating directory: %s\n", archive_error_string(a));
        }
        if (r < ARCHIVE_WARN) {
            archive_read_free(a);
            close(infd);
            return -1;
        }
        count++;
    }

    archive_read_free(a);
    close(infd);
    return count;
}

static inline void status(const char *fmt, ...)
{
    va_list ap;
    if (opt_quiet == true) return;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fflush(stderr);
    va_end(ap);
}

static void kill_children(pid_t children[static 1], int nchildren)
{
    for (int i = 0; i < nchildren; i++) {
        if (children[i] != 0) {
            kill(children[i], SIGTERM);
        }
    }
}

static int copy_data(struct archive *ar, struct archive *aw)
{
	int r;
	const void *buff;
	size_t size;
	int64_t offset;

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);
		if (r == ARCHIVE_EOF)
			return (ARCHIVE_OK);
		if (r != ARCHIVE_OK) {
			status("error reading compressed data: %s\n", archive_error_string(ar));
			return (r);
		}
		r = archive_write_data_block(aw, buff, size, offset);
		if (r != ARCHIVE_OK) {
			status("error writing decompressed data:%s\n", archive_error_string(ar));
			return (r);
		}
	}
}

static int extract_file(struct archive *a, struct archive_entry *e)
{
    struct archive *ext = archive_write_disk_new();
    int r;
    int flags = 0;

    flags |= ARCHIVE_EXTRACT_TIME;
    flags |= ARCHIVE_EXTRACT_PERM;
    flags |= ARCHIVE_EXTRACT_ACL;
    flags |= ARCHIVE_EXTRACT_FFLAGS;

    archive_write_disk_set_options(ext, flags);
    r = archive_write_header(ext, e);
    if (r != ARCHIVE_OK) {
        status("error extracting: %s\n", archive_error_string(a));
        archive_write_free(ext);
        return r;
    }

    r = copy_data(a, ext);
    archive_write_free(ext);

    return r;    
}

/* A better data structure might make sense, but we're unlikely to have many
 * children anyway.
 */
static inline void add_child(pid_t children[static 1], int nprocs, pid_t child)
{
    for (int i = 0; i < nprocs; i++) {
        if (children[i] == 0) {
            children[i] = child;
            return;
        }
    }
    assert("attempt to add too many children" == 0);
}

static inline void del_child(pid_t children[static 1], int nprocs, pid_t child)
{
    for (int i = 0; i < nprocs; i++) {
        if (children[i] == child) {
            children[i] = 0;
            return;
        }
    }
    assert("attempt to remove nonexistant child" == 0);
}

static int extract(const char fn[restrict 1])
{
    struct archive *a;
    struct archive_entry *e;
    int r;
    int nchildren = 0; /* current number of running children */
    pid_t *children; /* array of running children */
    int infd;
    int numfiles = get_num_files(fn);
    int completed = 0;

    if (numfiles < 0) {
        return EXIT_FAILURE;
    }

    if ((children = calloc(opt_nprocs, sizeof(pid_t))) == NULL) {
        fprintf(stderr, "memory allocation error\n");
        return EXIT_FAILURE;
    }

    a = archive_read_new();
    archive_read_support_filter_all(a);
    archive_read_support_format_all(a);

    infd = open(fn, O_RDONLY);
    if (infd == -1) {
        fprintf(stderr, "unable to open %s (%s)\n", fn, strerror(errno));
        archive_free(a);
        free(children);
        return EXIT_FAILURE;
    }

    if (archive_read_open_fd(a, infd, 1024 * 8) != ARCHIVE_OK) {
        fprintf(stderr, "unable to open %s: %s\n", fn, archive_error_string(a));
        close(infd);
        archive_free(a);
        free(children);
        return EXIT_FAILURE;
    }

    status("extracting %s using up to %d CPUs\n", fn, opt_nprocs);

    while (true) {
        r = archive_read_next_header(a, &e);
        if (r == ARCHIVE_EOF) {
            break;
        }
        if (r == ARCHIVE_RETRY) {
            continue;
        }
        if (r < ARCHIVE_OK) {
            fprintf(stderr, "error enumerating directory: %s\n", archive_error_string(a));
        }
        if (r < ARCHIVE_WARN) {
            archive_free(a);
            kill_children(children, opt_nprocs);
            free(children);
            return EXIT_FAILURE;
        }

        mode_t type = archive_entry_filetype(e);
        if (S_ISDIR(type)) {
            const char *dirname = archive_entry_pathname(e);
            status("  creating %s [%d/%d]\n", dirname, completed + 1, numfiles);
            completed++;
            extract_file(a, e);
            continue;
        }

        status("extracting %s [%d/%d]\n", archive_entry_pathname(e), completed + 1, numfiles);
        completed++;

        if (opt_nprocs == 1) {
            extract_file(a, e);
        } else {
            if (nchildren >= opt_nprocs) {
                /* too many workers already, wait for one to complete */
                int wstatus;
                pid_t child = wait(&wstatus);
                if (WIFEXITED(wstatus)) {
                    nchildren--;
                    del_child(children, opt_nprocs, child);
                } else {
                    fprintf(stderr, "child %d exited unexpectedly with signal %d\n",
                        child, WTERMSIG(wstatus));
                    return EXIT_FAILURE;
                }
            }

            off_t loc = lseek(infd, 0, SEEK_CUR);

            pid_t child = fork();
            if (child == -1) {
                fprintf(stderr, "unable to fork child: %s\n", strerror(errno));
                archive_free(a);
                kill_children(children, opt_nprocs);
                free(children);
                return EXIT_FAILURE;
            }

            if (child == 0) {
                int newfd = open(fn, O_RDONLY);
                close(infd);
                dup2(newfd, infd);
                close(newfd);
                lseek(infd, loc, SEEK_SET);
                extract_file(a, e);
                archive_free(a);
                free(children);
                exit(EXIT_SUCCESS);
            } else {
                nchildren++;
                add_child(children, opt_nprocs, child);
            }
        }
    }

    /* wait for any remaining children to finish */
    if (opt_nprocs > 1) {
        for (int i = nchildren; i > 0; i--) {
            int wstatus;
            status("\r%d file%s still unzipping... ", i, i > 1 ? "s" : "");
            pid_t death = wait(&wstatus);
            if (WIFEXITED(wstatus) == 0) {
                fprintf(stderr, "child %d exited unexpectedly with signal %d\n",
                    death, WTERMSIG(wstatus));
            }
        }
        status("\n");
    }

    free(children);
    archive_free(a);

    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    const char *archive = NULL;
    const char *opt_dir = NULL;
    const char *opts = "d:hlqj::";
    int opt;

    opt_nprocs = get_nprocs(4);

    if (argc < 1) {
        usage(stderr, argv[1]);
        exit(EXIT_FAILURE);
    }

    while ((opt = getopt(argc, argv, opts)) != -1) {
        switch (opt) {
        case 'd':
            opt_dir = optarg;
            break;
        case 'h':
            usage(stdout, argv[0]);
            exit(EXIT_SUCCESS);
            break;
        case 'q':
            opt_quiet = true;
            break;
        case 'l':
            opt_listonly = true;
            break;
        case 'j':
            if (optarg == 0) {
                opt_nprocs = sizeof(cpu_set_t) * 8;
            } else {
                opt_nprocs = atoi(optarg);
            }

            if (opt_nprocs < 1) {
                fprintf(stderr, "invalid CPU count specified to -j\n");
                usage(stderr, argv[0]);
                exit(EXIT_FAILURE);
            }

            break;
        default:
            usage(stderr, argv[0]);
            exit(EXIT_FAILURE);
            break;
        }
    }

    if (optind >= argc) {
        usage(stderr, argv[0]);
        exit(EXIT_FAILURE);
    }

    archive = realpath(argv[optind], NULL);

    if (archive == NULL) {
        fprintf(stderr, "Can't turn '%s' in to a realpath: %s\n", argv[optind], strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (opt_dir != NULL) {
        if (chdir(opt_dir) == -1) {
            fprintf(stderr, "Failed to change directory to '%s': %s\n", opt_dir, strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    exit(extract(archive));
}
