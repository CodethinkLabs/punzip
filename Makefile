punzip: punzip.c
	$(CC) -O2 -Wall -pedantic -Wextra -o punzip -D_GNU_SOURCE punzip.c -larchive

clean:
	rm -rf punzip

